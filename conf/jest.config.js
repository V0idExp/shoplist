const path = require('path');

module.exports = {
    rootDir: path.join(process.cwd(), 'shoplist-frontend'),
    collectCoverage: true,
    verbose: true
};
