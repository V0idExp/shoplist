from os import environ
from subprocess import call


if __name__ == '__main__':
    environ['FLASK_APP'] = 'shoplist/__init__.py'
    environ['FLASK_DEBUG'] = '1'
    call(['flask', 'run'])
