Shop list - a simple Python web application demo
================================================


# Backend

## Virtualenv setup
It's recommended to install the project dependencies in a separate Python virtual environment.
The easiest way to go:

    python3 -m venv env
    env/bin/activate.sh       # on Linux/OSX:
    env/Scripts/Activate.bat  # on Windows CMD
    env/Scripts/Activate.ps1  # on Windows PowerShell

## Dependencies
The backend has a number of dependencies, easily installable with

    pip install -r requirements.txt


## Running
Once the virtual environment _is activated_, run the Flask web application by executing

    python run.py


# Frontend

## Dependencies
The frontend is developed on Node.js framework and uses Yarn for package management on top of it.
To grab and setup everything at once, just type:

    yarn

## Running
To start the development webpack live server:

    yarn start

The frontend webapp will be then accessible at http://localhost:9000/

## Building
To build production-ready deployable artifacts:

    yarn build