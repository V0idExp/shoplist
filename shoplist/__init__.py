from flask import Flask
import json
from flask import request


app = Flask('shoplist')

SHOPLIST = {
    '19999': {
        'name': 'Shopping list #1',
        'items': {
            '111': {
                'name': 'milk',
                'checked': False,
            },
            '222': {
                'name': 'eggs (3)',
                'checked': True,
            },
            '333': {
                'name': 'sushi (the good one)',
                'checked': False,
            }
        }
    }
}

SHOPLIST_COUNTER = 12000

# SHOPLIST = {
#     "name":"description"
#     "item1":"smth"
#     "item2":"nothing"
#     "item3":"anything"
# }


@app.route('/api/v1/lists', methods=['GET'])
def get_lists():
    return json.dumps(SHOPLIST)


@app.route('/api/v1/lists', methods=['POST'])
def create_list():
    global SHOPLIST
    global SHOPLIST_COUNTER

    data = json.loads(request.data)
    name = data['name']

    list_id = str(SHOPLIST_COUNTER)
    SHOPLIST_COUNTER += 1

    list_entry = {
        'name': name,
        'items': [],
    }

    SHOPLIST[list_id] = list_entry

    return json.dumps({
        'id': list_id,
    })

    # if request_method == 'POST':
    #     list = request.files['the_file']
    #     list.save()
    #     return ''


@app.route('/api/v1/lists/<list_id>', methods=['POST'])
def add_item(list_id):
    global SHOPLIST

    data = json.loads(request.data)
    items = data['items']

    list_entry = {
        'items': items,
    }

    SHOPLIST[list_id] = list_entry

    return json.dumps({
        'items': items,
    })


@app.route('/api/v1/lists/<list_id>/<item_id>', methods=['PUT'])
def update_item(list_id, item_id):
    global SHOPLIST

    data = json.loads(request.data)
    items = data['items']
    checked = bool(data['checked'])

    list_entry = {
        'items':items,
        'checked':checked,
    }

    SHOPLIST[list_id,item_id] = list_entry

    return json.dumps({
        'items':items
    })


@app.route('/api/v1/lists/<list_id>/<item_id>', methods=['DELETE'])
def delete_item(list_id, item_id):
    return ''
