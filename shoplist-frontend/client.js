const DUMMY_LISTS = [
	{
		id: 222,
		name: 'Sunday party',
		items: [
			{
				id: 123,
				name: 'Vodka',
				checked: false,
			},
			{
				id: 124,
				name: 'Soda',
				checked: true,
			},
			{
				id: 125,
				name: 'Hamburgers (4)',
				checked: false,
			},
		]
	},
	{
		id: 333,
		name: 'Angry Dasha starter pack',
		items: [
			{
				id: 223,
				name: 'Бесишь меня',
				checked: true,
			},
			{
				id: 224,
				name: 'Ты сейчас получишь!!11',
				checked: false,
			},
			{
				id: 225,
				name: 'та мне пофиг',
				checked: true,
			},
			{
				id: 226,
				name: 'трям',
				checked: true,
			},
		]
	}
];


export const fetchLists = () => {
	return DUMMY_LISTS;
};