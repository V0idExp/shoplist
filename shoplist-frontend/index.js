import './index.scss';
import { fetchLists } from './client';
import * as mustache from 'mustache';

const listTmpl = document.getElementById('list-tmpl').innerHTML;
mustache.parse(listTmpl);

const $lists = document.getElementById('lists-container');

$lists.innerHTML = mustache.render(listTmpl, {lists: fetchLists()});

